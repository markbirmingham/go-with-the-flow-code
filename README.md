# HBaaS Server

This is a fun demo API for saying happy birthday to people for the purposes of demonstrating server containerisation and deployment.

## Software Stack

This project uses the [Go programming language](https://go.dev/) for rapid prototype development and easy containerisation.
 
For the RESTful API, we are using the [Echo](https://echo.labstack.com/) framework due to its ease-of-use, minimalism, extensability but also performance. 

## Running

To run the server, simply do:

```shell script
./hbaas-server
```

(This is equivalent to running `./hbaas-server run-server`.)

For information on all command-line options, run:

```sh
./hbaas-server --help
```

To get version information only, simply run:

```sh
./hbaas-server version
```
